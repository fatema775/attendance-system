<?php
	require_once '../Library/authentication.php';
	session_start();
	sessionAuthenticate();
	if(isset($_GET["page"])){
		$page = $_GET["page"];
	}
	if(isset($_GET["sem"])){
		$sem = $_GET["sem"];
	}
	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Attendance System</title>
		<link rel="stylesheet" href="../style.css">
	</head>
	<body>
		<div class="hdr">
			<p>Attendance Pannel</p>
		</div>
		<div class="nav">
			<ul>
  				<li><a href="index.php?page=class">Class Information</a></li>
  				<li class="dropdown"><a href="javascript:void(0) class="dropbtn">View Attendance</a>
    				<div class="dropdown-content">
						<a href="index.php?page=view&sem=1st">1st Semester</a>
					  	<a href="index.php?page=view&sem=3rd">3rd Semester</a>
						<a href="index.php?page=view&sem=5th">5th Semester</a>
						<a href="index.php?page=view&sem=7th">7th Semester</a>
					</div></li>
  				<li class="rnav"><a href="index.php?page=logout">Logout</a></li>
			</ul>
		</div>
		<div>
			<center><?php require_once 'controller.php'; ?></center>
		</div>
	</body>
</html>