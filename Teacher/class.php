<?php
	require_once '../Library/dbConnect.php';

	$id = $_SESSION['id'];
	date_default_timezone_set("Asia/Dhaka");
	$day = date('l');

	$sql = "SELECT * FROM `course`, `class` WHERE t_id='$id' AND course.c_id=class.c_id AND day='$day' ORDER BY s_time ";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    $no = $result->num_rows;
	    if ($no == 1) echo "<p>Today You Have ".$no." Class</p>";
	    else echo "<p>Today You Have ".$no." Classes</p>";
		echo "<table><tr><th colspan=2>Class Time</th><th rowspan=2>Course Name</th><th rowspan=2>Department</th><th rowspan=2>Semester</th><th rowspan=2>Number of Students</th><th rowspan=2>Action</tr><tr><th>Start Time</th><th>End Time</th></tr>";
		while($row = $result->fetch_assoc()){
        	echo "<tr><td>" . $row["s_time"]. "</td><td>" . $row["e_time"]. "</td><td>" . $row["c_name"]. "</td><td>" . $row["dept"]. "</td><td>" . $row["sem"]. "</td>";
        	$sem = $row["sem"];
        	$sql_cnt = "SELECT COUNT(s_id) AS stdno FROM `student` WHERE sem='$sem' ";
        	$rslt = $conn->query($sql_cnt);
        	$std = $rslt->fetch_assoc();
        	echo "<td>" . $std["stdno"]. "</td><td>";?>
        	<a class="take" href="attendance.php?sem=<?php echo $sem;?>&cid=<?php echo $row["c_id"];?>">Take Attendance</a>
        	<?php echo "</td><tr>";
	    }
	    echo "</table>";
	}
	else {
		echo "<p>Today You Have No Classes</p>";
	}
?>