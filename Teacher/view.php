<?php
	require_once '../Library/dbConnect.php';

	echo "<p>Total Attendance Scenario of ".$sem." Semester</p>";
	$sql = "SELECT * FROM `course` WHERE sem = '$sem' ORDER BY c_id";
	$result = $conn->query($sql);
	echo "<table><tr><th rowspan=2>Student ID</th><th rowspan=2>Student Name</th>";
	if($sem=="5th") echo "<th colspan=6>Course Name</th></tr>";
	else echo "<th colspan=7>Course Name</th></tr><tr>";
	while($row = $result->fetch_assoc()){
		$crs[] = $row["c_id"];
		echo "<th>" .$row["c_name"]. "</th>";
	}
	echo "</tr>";
	$sql = "SELECT s_id,s_name FROM `student` WHERE sem = '$sem' ";
	$result = $conn->query($sql);
	while($row = $result->fetch_assoc()){
		$id=$row["s_id"];$var="P";
		echo "<tr><td>" . $row["s_id"]. "</td><td>" . $row["s_name"]. "</td>";
		foreach ($crs as $val) {
			$sql_t = "SELECT COUNT(DISTINCT `date`) AS cnt FROM `attendance` WHERE `c_id`='$val'";
			$rslt_t = $conn->query($sql_t);
			$row_t= $rslt_t->fetch_assoc();
			$t = $row_t["cnt"];
			if($t==0) echo "<td>NULL</td>";
			else{
				$sql_p = "SELECT COUNT(`status`) AS cnt FROM `attendance` WHERE c_id='$val' AND s_id='$id' AND status='$var' ";
				$rslt_p = $conn->query($sql_p);
				$row_p= $rslt_p->fetch_assoc();
				$p = $row_p["cnt"];
				$pr = (float) ($p*100)/$t;
				$pr = number_format($pr,2);
				echo "<td>".$pr."%</td>";
			}
		}
		echo "</tr>";
	}
	echo "</table>";
?>